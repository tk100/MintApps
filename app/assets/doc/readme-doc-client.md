## MintApps Client - Documentation

### Overview

The MintApps client is based on the programming language JavaScript and can be used via a network using any modern browser. The MintApps are completely implemented in Vanilla JS, i.e. they do not require any framework. Accordingly, the layout is based exclusively on pure CSS.

### Directories

The app-directory contains the complete for running the MintApps, including all assets 
- assets: Assets (images, css, etc.)
  - data: Optional, contents of data repository
  - doc: Optional, automatically generated documentation
  - flags: Images of flags used in language selection
  - icons: Icons that are directly integrated using vite
  - libs: specific libraries are used by individual apps, e.g. to create QR codes
  - music: Songs used in some game apps
  - sounds: Sounds used in some game apps
  - styles: Central files for the layout
  - weblate: Contents of weblate repository
- components: Central web-components used by multiple apps, such as modal dialogs or menus. These are all standard Java Script classes. The Mintapps remain on the "light side of the force" and do not use shadow DOM. Find their documentation in section **Classes**.
- modules: Central libraries, e.g. for drawing graphics or for displaying formulas. These are standard ES6 JavaScript modules with named exports. Find their documentation in section **Modules**.
- html: Individual HTML pages for each app

### Configuration

- config.js: Main configuration file
- sitemap.js: Configuration of menus and individual apps
- eslint.config.js: Linter configuration
