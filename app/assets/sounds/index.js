const index = {
  author: 'Thomas Kippenberg',
  url: 'https://www.kippenbergs.de',
  items: {
    popSound: {
      file: './pop-sound.mp3',
      title: 'Short sound, used when removing items'
    },
    pushSound: {
      file: './push-sound.mp3',
      title: 'Short sound, used when adding items'
    },
    cord: {
      file: './cord.mp3',
      title: 'Short sound, C-major cord'
    }
  }
}

export default index
