const index = {
  author: 'John Bartmann',
  url: 'https://youtube.com/johnbartmannmusic',
  items: {
    lobby: {
      file: './John Bartmann - Let\'s Skate The Prom.mp3',
      title: 'Let\'s Skate The Prom'
    },
    report: {
      file: './John Bartmann - Safe House.mp3',
      title: 'Safe House'
    },
    ask0: {
      file: './John Bartmann - Lofi Jam.mp3',
      title: 'Lofi Jam'
    },
    ask1: {
      file: './John Bartmann - African Moon.mp3',
      title: 'African Moon'
    },
    ask2: {
      file: './John Bartmann - Umlungu.mp3',
      title: 'Umlungum'
    }
  }
}

export default index