import { MintCard } from './mint-card-components.js'
import './wait-icon.js'
import './alert-box.js'
import './form-components.js'
import { sInit, sRunning, sPaused, sDone } from '../modules/states.js'
import { emit, gid, showIf, importCss } from '../modules/utils.js'
import { formatNumber } from '../modules/plot-2d.js'
importCss('video-recorder.css')

const dt = 1000 / 30 // timer interval, not relevant for recording itself

/**
 * Card that contains a simple video recorder and basic settings
 * @property {Number} [data-initial-framreate=30] - initial value for framerate
 * @property {Number} [data-initial-direction=0] - initial camera direction
 * @property {Boolean} [data-audio=false] - record audio
 * @fires cancel - User canceled recording
 * @fires blob - User accepted recording
 */
class VideoRecorder extends MintCard {
  static observedAttributes = ['data-hide']

  #error = '' // error text
  #state = -1 // internal state of component
  #time = 0 // current time
  #maxTime = 10000 // maximum recording time in ms
  #mimeType = '' // video mime type
  #direction = 0 // camera orientation, 0 = default, 1 = front, 2 = rear
  #framerate = 30 // camera framerate
  #audio = false // record audio
  #timer = null // timer object
  #support = false // browser supports video recordings
  #captureStream = null // capture stream
  #recorder // video recorder
  #data = [] // captured data

  connectedCallback() {
    // title
    this.setAttribute('data-title', 'VideoRecorder.title')
    super.connectedCallback()
    // body
    this.body.innerHTML = `
    <wait-icon id="vr-wait-icon" data-hide></wait-icon>
    <div id="vr-video-container" class="video-container">
      <div class="text-overlay" id="text-overlay"></div>
      <video id="vr-preview" autoplay muted class="video-preview">
    </div>
    <alert-box id="vr-alert-error" data-type="warning" data-hide></alert-box>
    <form class="form-container">
      <mint-label for="vr-direction" data-text="VideoRecorder.direction"></mint-label>
      <input-select id="vr-direction" data-options="VideoRecorder.direction-default, VideoRecorder.direction-user, VideoRecorder.direction-environment"></input-select>
      <mint-label for="vr-framerate" data-text="VideoRecorder.framerate" data-unit="fps"></mint-label>
      <input-select id="vr-framerate" data-options="10, 20, 30, 40, 50, 60"></input-slider>
    </form>
    `
    // footer
    this.footer.innerHTML = `
      <button id="vr-btn-start" class="btn btn-primary" data-i18n="General.btn-start"></button>
      <button id="vr-btn-pause" class="btn" data-i18n="General.btn-pause"></button>
      <button id="vr-btn-resume" class="btn" data-i18n="General.btn-continue"></button>
      <button id="vr-btn-accept" class="btn btn-primary" data-i18n="General.btn-accept"></button>
      <button id="vr-btn-cancel" class="btn btn-secondary" data-i18n="General.btn-cancel"></button>`
    this.footer.removeAttribute('data-hide')
    // events
    gid('vr-btn-start').onclick = () => this.start()
    gid('vr-btn-pause').onclick = () => this.pause()
    gid('vr-btn-resume').onclick = () => this.resume()
    gid('vr-btn-accept').onclick = () => this.accept()
    gid('vr-btn-cancel').onclick = () => emit(this, 'cancel')
    gid('vr-direction').oninput = () => this.settingsChanged()
    gid('vr-framerate').oninput = () => this.settingsChanged()
    // attributes
    this.#maxTime = this.dataset.maxTime ?? 10000
    this.#framerate = this.dataset.initialFramerate ?? 30
    this.#direction = this.dataset.initialDirection ?? 0
    this.#audio = this.hasAttribute('data-audio')
    this.updateGui()
    // check for video support
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia || !MediaRecorder) {
      this.#error = 'VideoRecorder.error-video-not-supported'
    } else {
      // determine supported type
      if (MediaRecorder.isTypeSupported('video/mp4')) this.#mimeType = 'video/mp4'
      else if (MediaRecorder.isTypeSupported('video/webm')) this.#mimeType = 'video/webm'
      else this.#error = 'VideoRecorder.error-video-not-supported'
    }
    this.#support = this.#error === ''
    this.updateGui()
  }

  attributeChangedCallback(attr) {
    if (attr === 'data-hide') {
      if (this.hasAttribute('data-hide')) {
        this.reset()
      } else if (this.#support) {
        this.startStream()
      }
    }
  }

  // update user interface
  updateGui() {
    gid('vr-wait-icon').showIf(this.#state === -1 && !this.#error)
    showIf(gid('vr-video-container'), !this.#error)
    if (this.#error) gid('vr-alert-error').show(this.#error)
    else gid('vr-alert-error').hide()
    showIf(gid('vr-btn-start'), this.#state === sInit)
    showIf(gid('vr-btn-pause'), this.#state == sRunning)
    showIf(gid('vr-btn-resume'), this.#state == sPaused && this.#time < this.#maxTime)
    showIf(gid('vr-btn-accept'), this.#state === sPaused)
    gid('vr-framerate', this.#framerate / 10 - 1)
    gid('vr-direction', this.#direction)
  }

  // check for video support
  init() {
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia || !MediaRecorder) {
      this.#error = 'VideoRecorder.error-video-not-supported'
      this.updateGui()
      return
    }
    // determine supported type
    if (MediaRecorder.isTypeSupported('video/mp4')) this.#mimeType = 'video/mp4'
    else if (MediaRecorder.isTypeSupported('video/webm')) this.#mimeType = 'video/webm'
    else {
      this.#error = 'VideoRecorder.error-video-not-supported'
      this.updateGui()
      return
    }
    // read props
    this.startStream()
  }

  // start stream
  startStream() {
    const preview = gid('vr-preview')
    // get stream and display preview
    const options = { video: {}, audio: this.#audio }
    if (this.#direction === 1) options.video.facingMode = { ideal: 'user' }
    if (this.#direction === 2) options.video.facingMode = { ideal: 'environment' }
    options.video.frameRate = { exact: this.#framerate }
    navigator.mediaDevices
      .getUserMedia(options)
      .then((stream) => {
        preview.srcObject = stream
        this.#captureStream = stream
        return new Promise((resolve) => (preview.onplaying = resolve))
      })
      .then(() => {
        this.#state = sInit
        this.#recorder = new MediaRecorder(this.#captureStream, { mimeType: this.#mimeType })
        this.#recorder.onstop = () => this.done()
        this.#recorder.ondataavailable = (event) => this.#data.push(event.data)
        this.updateGui()
      })
      .catch((e) => {
        console.debug(e)
        this.#error = 'VideoRecorder.error-video-not-allowed'
        this.updateGui()
      })
  }

  // stop stream
  stopStream() {
    this.#captureStream?.getTracks()?.forEach((track) => track.stop())
  }

  // start recording
  start() {
    this.#recorder.start()
    this.#state = sRunning
    this.#timer = setInterval(() => this.increaseTime(), dt)
    this.updateGui()
  }

  // pause recording
  pause() {
    this.#recorder.pause()
    this.#state = sPaused
    if (this.#timer) clearInterval(this.#timer)
      this.updateGui()
  }

  // resume recording
  resume() {
    if (this.#time < this.#maxTime) this.#recorder.resume()
    this.#state = sRunning
    this.#timer = setInterval(() => this.increaseTime(), dt)
    this.updateGui()
  }

  // accept recording
  accept() {
    this.#recorder.stop()
    this.updateGui()
  }

  // recording done
  done() {
    if (this.#time === 0) return
    if (this.#timer) clearInterval(this.#timer)
    this.#state = sDone
    const blob = new Blob(this.#data, { type: this.#mimeType })
    emit(this, 'blob', { blob, mimeType: this.#mimeType })
  }

  // reset
  reset() {
    this.#error = ''
    if (this.#timer) clearInterval(this.#timer)
    this.#time = 0
    this.stopStream()
    this.#data = []
  }

  // increase recording timer and update overlay
  increaseTime() {
    gid('text-overlay').innerHTML = `<em>t</em> = ${formatNumber(this.#time / 1000, 1)}`
    if (this.#time >= this.#maxTime && this.#state === sRunning) this.pause()
    else this.#time += dt
  }

  // change view direction or framerate
  settingsChanged() {
    this.#framerate = gid('vr-framerate').value * 10 + 10
    this.#direction = gid('vr-direction').value
    this.reset()
    this.startStream()
  }
}

customElements.define('video-recorder', VideoRecorder)