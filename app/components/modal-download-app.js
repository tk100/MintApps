import { ModalDialog } from './modal-dialog.js'
import config from '../config.js'
import { translateChildren } from '../modules/i18n.js'
import './form-components.js'
import { uid } from '../modules/utils.js'

/**
 * Modal dialog with information about downloading apps as HTML or SCORM packages
 * @see ModalDialog
 */
class ModalDownloadApp extends ModalDialog {
  agree = null // reference to agree switch

  connectedCallback() {
    if (this.disconnected) return
    super.connectedCallback()
    // title
    this.title.setAttribute('data-i18n', 'Menus.download')
    // body
    const id = window.mintapps.id
    const info = document.createElement('div')
    info.setAttribute('data-i18n', 'ModalDownloadApp.download-info')
    const warning = document.createElement('div')
    warning.classList.add('alert-warning')
    warning.setAttribute('data-i18n', 'ModalDownloadApp.download-warning')
    // body → form
    const form = document.createElement('form')
    form.classList.add('form-container')
    this.agree = document.createElement('input-switch')
    this.agree.oninput = (e) => this.inputAgree(e)
    this.agree.id = uid()
    const label = document.createElement('mint-label')
    label.setAttribute('data-text', 'ModalDownloadApp.i-agree')
    label.setAttribute('for', this.agree.id)
    label.classList.add('wide-label')
    form.append(label, this.agree)
    this.body.append(info, warning, form)
    // footer
    const aScorm = document.createElement('a')
    aScorm.href = `${config.downloadPath}${id}.zip`
    aScorm.classList.add('btn', 'btn-primary', 'd-none')
    aScorm.setAttribute('data-i18n', 'Menus.download')
    aScorm.addEventListener('click', () =>  this.hide())
    /*
    const aHtml = document.createElement('a')
    aHtml.href = `${config.downloadPath}${id}-html.zip`
    aHtml.classList.add('btn', 'btn-primary', 'd-none')
    aHtml.setAttribute('data-i18n', 'ModalDownloadApp.download-html')
    */
    const btnClose = document.createElement('button')
    btnClose.classList.add('btn', 'btn-secondary')
    btnClose.setAttribute('data-i18n', 'General.btn-close')
    btnClose.onclick = () => this.emitClose()
    this.footer.append(aScorm, btnClose)
    this.footer.removeAttribute('data-hide')
    // translate
    translateChildren(this)
  }

  /**
   * Update the display when the agree button is pressed
   */
  inputAgree() {
    if (!this.footer) return
    this.footer.querySelectorAll('a').forEach(a => {
      if (this.agree.value) a.classList.remove('d-none')
      else a.classList.add('d-none')
    })
  }

}

// Register element
customElements.define('modal-download-app', ModalDownloadApp)