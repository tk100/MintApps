import config from '../config.js'
import { sitemap } from '../sitemap.js'
import { translateChildren, allLocales, setLocale } from '../modules/i18n.js'
import { enableEnterToClick, isReading, readNode, stopReader } from '../modules/accessibility.js'
import { allThemes, getTheme, setTheme } from '../modules/themes.js'
import { allFontsizes, setFontsize } from '../modules/fontsizes.js'
import { getMintAppsId, importCss, gid, showIf, buildHref, getMintAppsContainer } from '../modules/utils.js'
import { HTMLMintElement } from './abstract-components.js'
import { isValidSession, terminateSession } from '../modules/session.js'
import * as storage from '../modules/storage.js'
importCss('navbars.css')

/**
 * Top navigation bar, including submenus for settings, actions and users
 */
class NavbarTop extends HTMLMintElement {
  #showMenu = false // Kind of submenu to show, none if false
  #microNavigation = [] // Entries of micronavigation, only visible on large screens
  #modalShare = null // Modal share dialog
  #modalDownloadApp = null // Modal download dialog
  #navbarTopLogin = null // Navbar top login dialog

  #menu = [
    { id: 'menu-settings', text: 'General.settings', onclick: () => this.toggleMenu('settings') },
    { id: 'menu-page', text: 'General.page', onclick: () => this.toggleMenu('page') },
    { id: 'menu-login', text: 'User.login', onclick: () => this.toggleMenu('login') },
    { id: 'menu-user', text: 'User.user', onclick: () => this.toggleMenu('user') }
  ]
  #pageMenu = [
    { id: 'menu-page-search', icon: 'search.svg', text: 'General.search', href: 'mint-search.html' },
    { id: 'menu-page-print', icon: 'print.svg', text: 'General.print', onclick: () => this.actionPrint() },
    { id: 'menu-page-screen-reader', icon: 'screen-reader.svg', text: 'ScreenReader.read', onclick: () => this.actionScreenReader() },
    { id: 'menu-page-download', icon: 'download.svg', text: 'Menus.download', onclick: () => this.actionDownload() },
    { id: 'menu-page-share', icon: 'share.svg', text: 'General.share', onclick: () => this.actionShare() }
  ]
  #userMenu = [
    { id: 'menu-user-admin', icon: 'settings.svg', text: 'UserAdmin.title', href: 'mint-user-admin.html' },
    { id: 'menu-user-activities', icon: 'data.svg', text: 'General.activities', href: 'mint-user-data.html' },
    { id: 'menu-user-profile', icon: 'profile.svg', text: 'UserProfile.title', href: 'mint-user-profile.html' },
    { id: 'menu-user-logout', icon: 'logout.svg', text: 'User.logout', onclick: () => this.actionLogout() }
  ]

  constructor() {
    super()
    this.buildMicroNavigation()
  }

  connectedCallback() {
    if (this.disconnected) return
    window.addEventListener('keydown', (e) => this.keyListener(e))
    this.renderTopPanel()
  }

  /**
   * Create entries of micro navigation by comparing the current path with configured href attributes in sitemap.json
   */
  buildMicroNavigation() {
    const topics = []
    const microNavigation = [{ title: 'Menus.home', href: buildHref(sitemap.id) }]
    const id = getMintAppsId()
    for (const topic of sitemap.topics) {
      topics.push({ title: topic.title, href: buildHref(topic.id), icon: topic.icon })
      if (topic.id === id) {
        microNavigation.push({ title: topic.title, href: buildHref(topic.id) })
      } else if (topic.subtopics) {
        for (const subtopic of topic.subtopics) {
          for (const site of subtopic.sites) {
            if (site.id === id && microNavigation.length < 2) {
              microNavigation.push({ title: topic.title, href: buildHref(topic.id) })
              microNavigation.push({ title: site.title, href: buildHref(site.id) })
            }
          }
        }
      }
    }
    this.#microNavigation = microNavigation
  }

  /**
   * Listen to key events, esc closes menu
   * @param {Event} e
   */
  keyListener(e) {
    if (e.keyCode === 27) this.closeMenu()
  }

  /**
   * Hide menu when active, else show specified type
   * @param {String} type 
   */
  toggleMenu(type) {
    if (this.#showMenu === type) this.closeMenu()
    else {
      this.closeMenu()
      this.#showMenu = type
      this.renderPopupMenu()
      //adjustHeight()
    }
  }

  /**
   * Close submenu(s)
   */
  closeMenu() {
    this.#showMenu = false
    document.getElementById('menu-mask')?.remove()
    this.updateTopPanel()
  }

  /**
   * Generate top navbar panel (but not the popup menu)
   */
  renderTopPanel() {
    // home icon
    const homeIconImg = document.createElement('img')
    homeIconImg.setAttribute('data-i18n-alt', 'Menus.home')
    homeIconImg.setAttribute('data-i18n-title', 'Menus.home')
    homeIconImg.src = `../assets/icons/home.svg`
    homeIconImg.classList.add('nav-img', 'invert-dark')
    const homeIcon = document.createElement('a')
    homeIcon.classList.add('hide-large')
    homeIcon.href = buildHref(sitemap.id)
    homeIcon.append(homeIconImg)
    // micro navigation
    let microNav = document.createElement('span')
    microNav.classList.add('hide-small')
    this.#microNavigation.forEach(entry => {
      const a = document.createElement('a')
      a.classList.add('nav-link', 'micro-nav-link')
      a.href = entry.href
      a.setAttribute('data-i18n', entry.title)
      microNav.append(a)
    })
    // settings, page and login menu
    const span = document.createElement('span')
    this.#menu.forEach(entry => {
      const a = document.createElement('a')
      a.id = entry.id
      a.classList.add('nav-link', 'enter-click')
      a.onclick = entry.onclick
      a.setAttribute('data-i18n', entry.text)
      span.append(a)
    })
    // root
    this.classList.add('nav-top')
    this.append(homeIcon, microNav, span)
    translateChildren(this)
    // accessability
    enableEnterToClick(this)
    this.updateTopPanel()
  }

  /**
   * Update top panel (hide login or user menu)
   */
  updateTopPanel() {
    isValidSession().then(loggedIn => {
      showIf(gid('menu-user'), loggedIn)
      showIf(gid('menu-login'), !loggedIn)
    })
  }

  /**
   * Generate and show popup menu
   */
  async renderPopupMenu() {
    // menu mask
    const mask = document.createElement('div')
    mask.id = 'menu-mask'
    mask.classList.add('menu-mask')
    let container, actions
    switch (this.#showMenu) {
      // settings
      case 'settings':
        // theme settings
        container = document.createElement('div')
        container.classList.add('grid-container', 'theme-container')
        for (let [id, item] of Object.entries(allThemes)) {
          item.onclick = () => this.changeTheme(id)
          container.append(this.renderItem(item))
        }
        mask.append(container)
        // fontsize settings
        container = document.createElement('div')
        container.classList.add('grid-container', 'theme-container')
        for (let [id, item] of Object.entries(allFontsizes)) {
          item.onclick = () => this.changeFontsize(id)
          container.append(this.renderItem(item))
        }
        mask.append(container)
        // locale settings
        container = document.createElement('div')
        container.classList.add('grid-container', 'locale-container')
        for (let [id, item] of Object.entries(allLocales)) {
          item.onclick = () => this.changeLocale(id)
          container.append(this.renderItem(item, false, false))
        }
        mask.append(container)
        break;
      // page specific actions
      case 'page':
        container = document.createElement('div')
        container.classList.add('grid-container', 'action-container')
        actions = this.#pageMenu.filter(entry => entry.id !== 'menu-page-download' || (window.mintapps?.download && config.downloadPath))
        mask.append(container)
        actions.forEach(item => container.append(this.renderItem(item)))
        break;
      // login menu (separate component)
      case 'login':
        container = document.createElement('div')
        container.classList.add('grid-container')
        if (!this.#navbarTopLogin) {
          await import('./navbar-top-login.js')
          this.#navbarTopLogin = document.createElement('navbar-top-login')
          this.#navbarTopLogin.addEventListener('close', () => this.closeMenu())
        }
        container.append(this.#navbarTopLogin)
        mask.append(container)
        break
      // user menu
      case 'user':
        container = document.createElement('div')
        container.classList.add('grid-container', 'action-container')
        actions = this.#userMenu.filter(entry => entry.id !== 'menu-user-admin' || storage.get('session')?.admin)
        actions.forEach(item => container.append(this.renderItem(item)))
        mask.append(container)
        break;
      default:
        this.closeMenu()
    }
    this.append(mask)
    translateChildren(mask)
    enableEnterToClick(mask)
  }

  /**
   * Change theme
   * @param {String} theme - id of theme
   */
  changeTheme(theme) {
    setTheme(theme)
    this.closeMenu()
  }

  /**
   * Change fontsize
   * @param {String} fontsize - id of fontsize
   */
  changeFontsize(fontsize) {
    setFontsize(fontsize)
    this.closeMenu()
  }

  /**
   * Change language
   */
  changeLocale(locale) {
    setLocale(locale)
    this.closeMenu()
  }

  /**
   * Show modal download dialog
   */
  async actionDownload() {
    this.closeMenu()
    if (!this.#modalDownloadApp) {
      await import('./modal-download-app.js')
      this.#modalDownloadApp = document.createElement('modal-download-app')
      getMintAppsContainer().append(this.#modalDownloadApp)
    }
    this.#modalDownloadApp.show()
  }

  /**
   * Open print view and load light theme
   */
  actionPrint() {
    this.closeMenu()
    const oldTheme = getTheme()
    setTheme('light')
    window.print()
    setTheme(oldTheme)
  }

  /**
   * Show modal share dialog
   */
  async actionShare() {
    this.closeMenu()
    if (!this.#modalShare) {
      await import('./modal-share.js')
      this.#modalShare = document.createElement('modal-share')
      getMintAppsContainer().append(this.#modalShare)
    }
    const url = new URL(window.location.href)
    url.searchParams.delete('lang')
    url.searchParams.delete('room')
    this.#modalShare.setAttribute('data-url', url.toString())
    this.#modalShare.show()
  }

  /**
   * Toggle screen reeder
   */
  actionScreenReader() {
    this.closeMenu()
    if (isReading()) stopReader()
    else readNode(document.body)
  }

  /**
   * User logout
   */
  async actionLogout () {
    this.closeMenu()
    terminateSession().then(() => this.updateTopPanel())
  }

  /**
   * Render item of settings, action or user menu
   * @param {Object} item - info object {text, src, onlick, href}
   * @param {boolean} [translate=true] - wether to translate the label (not necessary for language names)
   * @param {boolean} [invertDark=true] - wether to invert colors for dark theme (not reasonabe for flags)
   * @returns {HTMLDivElement}
   */
  renderItem(item, translate = true, invertDark = true) {
    // image
    const img = document.createElement('img')
    img.classList.add('cursor-pointer')
    if (invertDark) img.classList.add('invert-dark')
    img.src = `../assets/icons/${item.icon}`
    if (translate) img.setAttribute('data-i18n', item.text)
    else img.alt = item.text
    // label
    const label = document.createElement('label')
    if (translate) label.setAttribute('data-i18n', item.text)
    else label.innerText = item.text
    // hyperlink
    const a = document.createElement('a')
    a.tabIndex = 0
    if (item.href) a.href = item.href
    if (item.onclick) a.onclick = item.onclick
    a.append(img)
    // entry
    const entry = document.createElement('div')
    entry.classList.add('entry')
    entry.append(a, label)
    return entry
  }
}

customElements.define('navbar-top', NavbarTop);
