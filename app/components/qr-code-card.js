import '../components/qr-code.js'
import '../components/form-components.js'
import '../components/alert-box.js'
import '../components/modal-download-file.js'
import '../components/qr-code.js'
import '../components/modal-download-file.js'
import { gid, showIf, getMintAppsContainer } from '../modules/utils.js'
import { MintCard } from './mint-card-components.js'
import * as plot2d from '../modules/plot-2d.js'
import { getBrowserName } from '../modules/browser.js'

/**
 * Card displaying an QR-Code with several options to style the code
 * @property {String} data-text - Text input
 */
class QrCodeCard extends MintCard {
  static observedAttributes = ['data-text']

  #qrContainer = null // reference to qr-container
  #qrCode = null // reference to qr-code
  #form = null // reference to form
  #alert = null // alert box
  #text = '' // text
  #pixelSize = 100 // qr code size in px
  #size = null // reference to size input
  #radius = null // reference to radius input
  #color = null // reference to color input
  #btnCopy = null // reference to copy button
  #btnSave = null // reference to save buttons
  #browser = getBrowserName() // browser name
  #modalDownloadFile = null // reference to modal download

  connectedCallback() {
    // title
    this.setAttribute('data-title', 'General.qr-code')
    super.connectedCallback()
    // body → qr code
    this.#qrContainer = document.createElement('div')
    this.#qrContainer.classList.add('text-center', 'w-100')
    this.#qrCode = document.createElement('qr-code')
    this.#qrCode.setAttribute('data-text', this.dataset.text)
    this.#qrContainer.append(this.#qrCode)
    this.body.append(this.#qrContainer)
    // body → alert
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-hide', '')
    this.body.append(this.#alert)
    // body → form
    this.#form = document.createElement('form')
    this.#form.classList.add('form-container')
    this.#form.innerHTML = `
      <mint-label for="qr-size" data-text="QrGenerator.size"></mint-label>
      <input-slider id="qr-size" data-min="0.4" data-max="0.9" data-step="0.01"></input-slider>
      <mint-label for="qr-radius" data-text="QrGenerator.radius"></mint-label>
      <input-slider id="qr-radius" data-min="0" data-max="1" data-step="0.1"></input-slider>
      <mint-label for="qr-color" data-text="General.color"></mint-label>
      <input-color id="qr-color"></input-color>
    `
    this.body.append(this.#form)
    this.#size = gid('qr-size', 0.4)
    this.#radius = gid('qr-radius', 0.1)
    this.#color = gid('qr-color', '#808080')
    // footer → buttons
    this.footer.innerHTML =
      `<button id="btn-qr-copy" class="btn" data-i18n="General.btn-copy-img"></button>
       <button id="btn-qr-save" class="btn" data-i18n="General.btn-save-img"></button>`
    this.#btnCopy = gid('btn-qr-copy')
    this.#btnSave = gid('btn-qr-save')
    this.#btnCopy.onclick = () => this.copyImage()
    this.#btnSave.onclick = () => this.saveImage()
    // modal download
    this.#modalDownloadFile = document.createElement('modal-download-file')
    getMintAppsContainer().append(this.#modalDownloadFile)
    // events
    this.#size.oninput = () => {
      this.#pixelSize = this.#qrContainer.clientWidth * this.#size.value
      this.#qrCode.setAttribute('data-size', this.#pixelSize)
    }
    this.#radius.oninput = () => {
      this.#qrCode.setAttribute('data-radius', this.#radius.value)
    }
    this.#color.oninput = () => {
      this.#qrCode.setAttribute('data-color', this.#color.value)
    }

  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'data-text') {
      this.#text = newValue
      if (this.#qrCode) {
        this.#qrCode.setAttribute('data-text', this.#text)
        this.update()
      }
    }
  }

  /**
   * Update qr code and view
   */
  update() {
    const oldPixelSize = this.#pixelSize
    this.#alert.hide()
    showIf(this.#form, this.#text)
    showIf(this.#qrContainer, this.#text)
    showIf(this.#btnCopy, this.#browser != 'firefox' && this.#browser != 'safari')
    showIf(this.footer, this.#text)
    this.#pixelSize = this.#qrContainer ? this.#qrContainer.clientWidth * this.#size.value : 100
    if (!this.#text) this.#alert.show('QrGenerator.info-text')
    else if (oldPixelSize === 0) this.repaint()
  }

  /**
   * Repaint (triggered from mint-card-container)
   */
  repaint() {
    this.#pixelSize = this.#qrContainer ? this.#qrContainer.clientWidth * this.#size.value : 100
    this.#qrCode.setAttribute('data-size', this.#pixelSize)
  }

  /**
   * download image, available in all browsers
   */
  saveImage() {
    const canvas = this.#qrContainer.getElementsByTagName('canvas')[0]
    this.#modalDownloadFile.open({ name: 'QR', type: 'canvas', data: canvas })
  }

  /*
   * copy image, only available in chrome and edge
   */
  async copyImage() {
    const canvas = this.#qrContainer.getElementsByTagName('canvas')[0]
    try {
      await plot2d.copyImage(canvas)
    } catch (e) {
      this.#alert.show(e.message)
    }
  }
}

customElements.define('qr-code-card', QrCodeCard)

