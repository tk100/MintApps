import { emit } from '../modules/utils.js'
import { play } from '../modules/sound-player.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * Progresbar bar, optional with timer
 * @property {Boolean} [data-sound=false] - play sound effects
 * @property {Boolean} [data-count-down=false] - count down (instead of count up)
 * @property {Boolean} [data-show-time=false] - show remaining / elapsed time
 * @fires timeout - timer reached value 0
 * @fires tick - remaining time in seconds
 */
class ProgressBar extends HTMLMintElement {
  #duration = 0 // duration of timer
  #time = 0 // current time
  #firstTimeStamp = 0 // start time of animation
  #bar = null // reference to bar
  #value = null // current progress
  #ontimeout = null // optional listener for timeout event
  #running = false // flag, if timer is runnning
  #ontick = null // optional listener for tick every second
  #countDown = false // flag, count downwards instead of upwards
  #showTime = false // flag, show info on remaining / elapsed time

  connectedCallback() {
    if (this.disconnected) return
    this.#countDown = this.hasAttribute('data-count-down')
    this.#showTime = this.hasAttribute('data-show-time')
    this.style = `
      background-color: var(--dividor-color);
      border-radius: var(--border-radius);
      border: solid 1px var(--dividor-color);
      display:block;
      height: 1.8rem;
      width: 100%;
    `
    this.#bar = document.createElement('div')
    this.#bar.style = `
      background-color: var(--ternary-color);
      border-radius: var(--border-radius);
      font-size: 1.5rem;
      color: var(--icon-text-color);
      text-align: center;
      width: 0;
      height:100%;
      `
    this.append(this.#bar)
  }

  disconnectedCallback() {
    this.#running = false
    super.disconnectedCallback()
  }

  /**
   * Start timer
   * @param {Number} duration
   */
  start(duration) {
    if (this.hasAttribute('data-sound')) play('cord')
    this.#duration = duration
    this.stop()
    this.#time = 0
    this.#value = 0
    this.#firstTimeStamp = 0
    this.#running = true
    this.showTime()
    requestAnimationFrame(ts => this.step(ts))
  }

  /**
   * Stop timer
   */
  stop() {
    this.#running = false
    this.#bar.style.width = this.#countDown ? '100%' : '0'
    if (this.hasAttribute('data-sound')) play('cord')
  }

  /**
   * Time step, internal method
   */
  step(timestamp) {
    if (this.#firstTimeStamp === 0) this.#firstTimeStamp = timestamp
    const lastTime = this.#time
    this.#time = (timestamp - this.#firstTimeStamp) / 1000
    if (Math.floor(lastTime) !== Math.floor(this.#time)) {
      emit(this, 'tick')
      if (this.#ontick) this.#ontick()
      this.showTime()
    }
    this.value = this.#time / this.#duration
    if (this.#time >= this.#duration) {
      if (this.#ontimeout) this.#ontimeout()
      emit(this, 'timeout')
      this.#running = false
    } else if (this.#running) {
      requestAnimationFrame(ts => this.step(ts))
    }
  }

  /**
   * Show time
   */
  showTime() {
    if (this.#showTime) {
      let text = ' '
      if (this.#countDown) {
        if (this.#time >= 0 && this.#duration >= 0) text = Math.round(this.#duration - this.#time)
      } else {
        if (this.#time >= 0) text = Math.round(this.#time)
        else text = 0
      }
      this.#bar.innerText = text
    }
  }

  /**
   * Set progress
   * @param {Number} p - progress (between 0 and 1)
   */
  set value(p) {
    this.#value = p
    this.#bar.style.width = this.#countDown ? `${100 - 100 * p}%` : `${100 * p}%`
  }

  /**
   * Get current process
   * @returns {Number} progress between 0 and 1
   */
  get value() {
    return this.#value
  }

  /**
   * Set timeout listener
   * @param {Function} listener
   */
  set ontimeout(listener) {
    this.#ontimeout = listener
  }

  /**
 * Set tick listener
 * @param {Function} listener
 */
  set ontick(listener) {
    this.#ontick = listener
  }
}

customElements.define('progress-bar', ProgressBar)
