import { translateChildren } from '../modules/i18n.js'
import { emit, gid, showIf } from '../modules/utils.js'
import * as http from '../modules/http.js'
import * as storage from '../modules/storage.js'
import { isValidSession } from '../modules/session.js'
import { HTMLMintElement } from './abstract-components.js'
import './form-components.js'
import './alert-box.js'
import './wait-icon.js'

class NavbarTopLogin extends HTMLMintElement {
  #state = '' // internal state
  #generalError = '' // general error (for example server not available)
  #wait = false // show wait icon

  async connectedCallback() {
    const html = `
      <!-- Terms of usage -->
      <div id="login-terms">
        <p><b data-i18n="User.terms"></b></p>
        <div data-i18n-md-block="User.terms-text"></div>
        <div class="text-right">
          <button id="login-next" class="btn btn-primary" data-i18n="General.btn-next"></button>
        </div>
      </div>
      <!-- General error -->
      <alert-box id="general-error" data-type="warning"></alert-box>
      <!-- login form -->
      <div id="login-form" class="login-form">
        <form class="form-container">
          <mint-label for="login-user" data-text="User.user"></mint-label>
          <input-text id="login-user" autocomplete="username"></input-text>
          <mint-label for="login-password" data-text="User.password"></mint-label>
          <input-text id="login-password" type="password" autocomplete="current-password"></input-text>
          <mint-label for="login-token" data-text="User.token"></mint-label>
          <input-text id="login-token" autocomplete="off"></input-text>
        </form>

        <!-- info password / username wrong -->
        <alert-box id="login-warning" data-type="warning" data-text="Errors.secret-invalid|Errors.totp-missing|Errors.totp-invalid"></alert-box>

        <!-- info wait -->
        <wait-icon id="login-wait" data-msg="Errors.user-login-wait"></wait-icon>

        <!-- info password reset -->
        <alert-box id="login-info" data-text="User.reset-password-info"></alert-box>

        <!-- info cookie -->
        
        <!-- login button-->
        <div id="login-button-container" class="login-button-container">
          <alert-box id="login-warning" data-text="User.cookie-info"></alert-box>
          <!--<div data-i18n-md-block="User.password-info"></div>-->
          <div class="text-right">
            <button id="login-button" class="btn btn-primary" data-i18n="User.login"></button>
          </div>
        </div>
      </div>
    </div>`
    this.innerHTML = html
    this.classList.add('login-container')
    await this.init()
    // events
    gid('login-button').onclick = () => this.login()
    gid('login-next').onclick = () => emit(this, 'close')
    gid('login-user').addEventListener('enter', () => this.login())
    gid('login-password').addEventListener('enter', () => this.login())
    gid('login-token').addEventListener('enter', () => this.login())
  }

  /**
   * Initialize dialog
   */
  async init() {
    if (await isValidSession()) {
      this.#state = 'error'
      this.#generalError = 'Errors.user-close-sessions'
    } else {
      this.#state = 'init'
      this.#generalError = ''
      gid('login-user').focus()
    }
    this.updateGui()
    translateChildren(this)
  }

  /**
   * Update user interface
   */
  updateGui() {
    // show or hide
    showIf(gid('login-terms'), this.#state === 'terms')
    showIf(gid('login-warning'), false)
    showIf(gid('general-error'), this.#generalError)
    showIf(gid('login-form'), this.#state !== 'terms' && this.#state !== 'error')
    showIf(gid('login-info'), false)
    showIf(gid('login-wait'), this.#wait)
    showIf(gid('login-button-container'), !this.#wait)
    gid('login-token').showIf(this.#state.startsWith('totp'))
    // show general error
    if (this.#generalError) gid('general-error').setAttribute('data-text', this.#generalError)
    // disable or enable elements
    gid('login-user').removeAttribute('disabled')
    gid('login-password').removeAttribute('disabled')
    gid('login-token').removeAttribute('disabled')
    if (this.#wait) {
      gid('login-user').setAttribute('disabled', '')
      gid('login-password').setAttribute('disabled', '')
      gid('login-token').setAttribute('disabled', '')
    }
    if (this.#state.startsWith('totp')) {
      gid('login-user').setAttribute('disabled', '')
      gid('login-password').setAttribute('disabled', '')
    }
    // show login errors
    let text, type
    switch (this.#state) {
      case 'secret-invalid': 
        text = 'Errors.secret-invalid'
        type = 'warning'
      break
      case 'totp-invalid': 
        text = 'Errors.totp-invalid'
        type = 'warning'
        break
      case 'totp-missing': 
        text = 'Errors.totp-missing'; 
        type = 'info';
        gid('login-token').focus()
      break
    }
    if (text) { 
      gid('login-warning').setAttribute('data-text', text)
      gid('login-warning').setAttribute('data-type', type)
      gid('login-warning').show()
    }
    translateChildren(this)
  }

  /**
   * Send login data to server and react to response
   */
  async login() {
    const user = gid('login-user')
    const password = gid('login-password')
    const token = gid('login-token')
    this.#generalError = ''
    try {
      const s = await http.post('sync:get-session', { user: user.value, secret: password.value, token: token.value })
      storage.set('session', {
        user: user.value,
        token: s.token,
        admin: s.admin,
        totp: s.totp,
        created: s.created,
        lastlogin: s.lastlogin
      })
      // first login - show terms
      if (s.lastlogin === 0) {
        this.#state = 'terms'
        this.updateGui()
      }
      else emit(this, 'close')
    } catch (e) {
      const msg = e.message
      if (msg === 'totp-missing') {
        this.#state = msg
      } else if (msg === 'secret-invalid' || msg === 'totp-invalid' || msg === 'user-login-wait') {
        this.#state = msg
        this.#wait = true
        setTimeout(() => {
          this.#wait = false
          this.updateGui()
        }, 5000)
      } else {
        this.#generalError = `Errors.${e.message}`
        this.#state = 'error'
      }
      this.updateGui()
    }
  }
}

customElements.define('navbar-top-login', NavbarTopLogin)
