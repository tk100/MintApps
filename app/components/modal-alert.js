import { translateElement } from '../modules/i18n.js';
import { HTMLMintElement } from './abstract-components.js';
import { importCss } from '../modules/utils.js';
importCss('modal-alert.css')

/**
 * Modal dialog that displays a message for a short time.
 * @property {Number} [data-timeout=3] - Length of time the message is displayed in seconds
 */
class ModalAlert extends HTMLMintElement {
  static observedAttributes = ['data-timeout']

  #timeout = 3 // timeout in seconds
  #timer = null // timer object
  #message = null // reference to message

  connectedCallback() {
    if (this.dataset.timeout > 0) this.#timeout = this.dataset.timeout
    this.hide()
    this.className = 'alert-wrapper'
    this.onclick = () => this.hide()
    this.#message = document.createElement('div')
    this.#message.classList.add('alert-content')
    this.append(this.#message)
    document.addEventListener('keydown', (e) => this.keyListener(e))
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'data-timeout') this.#timeout = newValue
  }

  /**
   * Open dialog and display message
   * @param {String} text - message to display
   */
  show(text) {
    this.#message.setAttribute('data-i18n', text)
    translateElement(this.#message)
    if (this.#timer) clearTimeout(this.#timer)
    this.#timer = setTimeout(() => this.hide(), this.#timeout * 1000)
    super.show()
  }

  keyListener(e) {
    if (e.keyCode === 27) {
      this.hide()
    }
  }
}

customElements.define('modal-alert', ModalAlert)