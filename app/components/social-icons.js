import config from '../config.js'
import { getBgVarStyle } from '../modules/color.js'
import { enableEnterToClick } from '../modules/accessibility.js'
import { importCss } from '../modules/utils.js'
import { HTMLMintElement } from './abstract-components.js'
importCss('social-icons.css')

/**
 * Display of the link to email, repository, RSS and Mastodon as round icons, embedded in the page title
 * @see PageTitle
 */
class SocialIcons extends HTMLMintElement {
  icons = [] // list of icons to display

  connectedCallback() {
    if (this.disconnected) return
    this.defineItems()
    this.renderItems()
  }

  /**
   * Define items according to config
   */
  defineItems () {
    this.icons = [
      {
        text: 'Mastodon',
        icon: `../assets/icons/mastodon.svg`,
        href: 'https://bildung.social/@mintapps/'
      },
      {
        text: 'Git-Repository',
        icon: `../assets/icons/git.svg`,
        href: 'https://codeberg.org/MintApps/'
      },
      {
        text: 'Weblate',
        icon: `../assets/icons/weblate.svg`,
        href: 'https://translate.codeberg.org/projects/mintapps/mintapps-basis-komponenten/'
      },
      {
        text: 'RSS-Feed',
        icon: `../assets/icons/rss.svg`,
        href: 'https://codeberg.org/MintApps/client/releases.rss'
      }
    ]
    if (config.mailto) {
      this.icons.unshift({
        text: 'E-Mail',
        icon: `../assets/icons/mail.svg`,
        href: `mailto:${config.mailto}`
      })
    }
  }

  /**
   * Render icons
   */
  renderItems () {  
    this.icons.forEach((icon, i) => {
      const img = document.createElement('img')
      img.src = icon.icon
      img.alt = icon.text
      const a = document.createElement('a')
      a.classList.add('social-icon')
      a.href = icon.href
      a.target = '_blank'
      a.title = icon.text
      a.style = getBgVarStyle(i, this.icons.length)
      a.append(img)
      this.append(a)
    })
    enableEnterToClick(this)
  }
}

customElements.define('social-icons', SocialIcons)