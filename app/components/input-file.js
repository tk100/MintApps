import { AbstractFormElement } from './form-components.js'
import './alert-box.js'
import { emit, prevent, showIf, loadScript } from '../modules/utils.js'
import { uid } from '../modules/utils.js'

/**
 * File upload dialog (as part of form)
 * @property {String} data-file-type - allowed file types (csv, image, json, video)
 * @property {Number} [data-max-size=0.5] - maximum size of files in MB
 * @property {Number} [data-max-dim=500] - maximum dimension of images in p
 * @fires input - user selected file
 */
class InputFile extends AbstractFormElement {
  #value = '' // file name of selected file
  #accept = '' // accept parameter for input
  #allowedTypes = [] // list of allowed file types
  #selectedFile = null // selected file
  #showPreview = null // show preview
  #showPasteArea = false //show paste area
  #maxSize = 0.5 // maximum file size in MB
  #maxDim = 500 // maximum dimension of images in px
  #error = '' // error message
  #errorParams // error message parameters

  #preview = null // reference to preview area
  #previewImg = null // reference to preview image
  #btnDelete = null // reference to delete button in preview
  #manualUpload = null // reference to manual upload 
  #upload = uid() // reference to upload area
  #dropZone = null // reference to drop zone
  #pasteArea = null // reference to area for pasting contents
  #dragClickInfo = null // reference to info text
  #dragInfo = null // reference to second info text
  #input = null // reference to input element
  #alert = null // reference to warning message

  static observedAttributes = ['data-file-type', 'data-hide', 'data-max-dim', 'data-max-size']

  connectedCallback() {
    if (this.disconnected) return
    // preview → image
    const container = document.createElement('div')
    container.classList.add('form', 'img-container', 'flex-grow')
    container.style.borderRight = 'none'
    this.#previewImg = document.createElement('img')
    container.append(this.#previewImg)
    // preview → delete button
    this.#btnDelete = document.createElement('button')
    this.#btnDelete.classList.add('btn', 'btn-input')
    this.#btnDelete.innerHTML = '<img src="../assets/icons/delete.svg" data-i18n-alt="General.btn-delete-img" data-i18n-title="General.btn-delete-img">'
    // preview
    this.#preview = document.createElement('div')
    this.#preview.classList.add('d-flex')
    this.#preview.append(container, this.#btnDelete)
    // upload → drop & paste zone
    this.#dropZone = document.createElement('div')
    this.#dropZone.classList.add('drop-zone', 'text-center')
    this.#dropZone.tabIndex = 0
    this.#pasteArea = document.createElement('div')
    this.#pasteArea.innerText = ' '
    this.#dragClickInfo = document.createElement('span')
    this.#dragClickInfo.setAttribute('data-i18n', 'FileUpload.drag-click-info')
    this.#dragInfo = document.createElement('span')
    this.#dragInfo.setAttribute('data-i18n', 'FileUpload.drag-info')
    this.#dropZone.append(this.#pasteArea, this.#dragClickInfo, this.#dragInfo)
    // upload → manual upload → icon
    const iconContainer = document.createElement('div')
    iconContainer.classList.add('btn', 'btn-input', 'vertical-center-image')
    iconContainer.setAttribute('data-i18n-title', 'General.select')
    const uploadImg = document.createElement('img')
    uploadImg.src = `../assets/icons/upload.svg`
    iconContainer.append(uploadImg)
    // upload → manual upload → input
    this.#input = document.createElement('input')
    this.#input.type = 'file'
    this.#input.accept = this.#accept
    this.#input.hidden = true
    this.#input.id = `mint-form-${this.id}`
    // upload → manual upload
    this.#manualUpload = document.createElement('label')
    this.#manualUpload.classList.add('text-center', 'd-flex')
    this.#manualUpload.tabIndex = 0
    this.#manualUpload.for = this.id
    this.#manualUpload.append(iconContainer, this.#input)
    // upload
    this.#upload = document.createElement('div')
    this.#upload.classList.add('d-flex')
    this.#upload.append(this.#dropZone, this.#manualUpload)
    // alert box
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-type', 'warning')
    // root with select / paste / input components
    this.classList.add('input-file')
    this.append(this.#preview, this.#upload, this.#alert)
    // events
    this.#btnDelete.onclick = (e) => this.deleteImage(e)
    this.#dropZone.onpaste = (e) => this.handlePaste(e)
    this.#dropZone.ondragenter = (e) => this.focusDropZone(e)
    this.#dropZone.ondragleave = (e) => this.unfocusDropZone(e)
    this.#dropZone.ondragover = (e) => this.focusDropZone(e)
    this.#dropZone.ondrop = (e) => this.handleDrop(e)
    this.#dropZone.onclick = (e) => this.handleDropClick(e)
    this.#manualUpload.ondrop = prevent
    this.#manualUpload.ondragover = prevent
    this.#input.oninput = (e) => this.fileSelected(e)
    // params
    this.#maxDim = this.dataset.maxDim
    this.#maxSize = this.dataset.maxSize
    // update gui
    this.classList.add('wide-element')
    this.updateGui()
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    super.attributeChangedCallback(attr, oldValue, newValue)
    switch (attr) {
      case 'data-file-type':
        this.#allowedTypes = []
        switch (newValue) {
          case 'csv': this.#allowedTypes.push('text/csv'); break
          case 'json': this.#allowedTypes.push('application/json'); break
          case 'video': this.#allowedTypes.push('video/mp4', 'video/webm'); break
          case 'image': this.#allowedTypes.push('image/apng', 'image/avif', 'image/gif', 'image/jpeg', 'image/png', 'image/svg+xml', 'image/webp'); break
        }
        this.#accept = this.#allowedTypes.join(',')
        if (this.#input) this.#input.accept = this.#accept
        break;
      case 'data-img':
        this.#selectedFile = newValue
        this.updateGui()
        break;
      case 'data-max-size':
        if (newValue > 0) this.#maxSize = newValue
        else this.#maxSize = 0.5
        break;
      case 'data-max-dim':
        if (newValue > 0) this.#maxDim = newValue
        else this.#maxDim = 500
        break;
    }
  }

  /**
   * show / hide elements according to current state
   */
  updateGui() {
    showIf(this.#preview, this.#showPreview)
    showIf(this.#upload, !this.#showPreview)
    showIf(this.#pasteArea, this.#showPasteArea)
    showIf(this.#dragClickInfo, !this.#showPasteArea && this.isTypeAccepted('image'))
    showIf(this.#dragInfo, !this.#showPasteArea && !this.isTypeAccepted('image'))
    showIf(this.#manualUpload, !this.#selectedFile)
    if (this.#error) this.#alert.show(this.#error, this.#errorParams)
    else this.#alert.hide()
  }

  /**
   * check if specific type ist accepted, default yes
   * @param {String} type - file type
   * @returns {Boolean}
   */
  isTypeAccepted(type) {
    return type === '*' || this.dataset.fileType === type || this.#allowedTypes.indexOf(type) >= 0
  }

  /**
   * Handle event, user has selected local file
   * @param {Event} event
   */
  fileSelected(event) {
    event.preventDefault()
    event.stopPropagation()
    const file = event.target.files[0]
    this.uploadFile(file)
  }

  /**
   * React to paste event from clipboard
   * @param {Event} ev 
   */
  handlePaste(event) {
    for (const item of event.clipboardData.items) {
      const file = item.getAsFile()
      if (file) {
        this.uploadFile(item.getAsFile())
        return
      }
    }
    this.#error = 'FileUpload.clipboard-no-file'
  }

  /**
   * Focus drop zone (similar to hover)
   */
  focusDropZone(event) {
    event.preventDefault()
    this.#dropZone.classList.add('drop-zone-focus')
  }

  /**
   * Unfocus drop zone (similar to hover)
   */
  unfocusDropZone(event) {
    event.preventDefault()
    this.#dropZone.classList.remove('drop-zone-focus')
  }

  /**
   * React to drop event from clipboard
   * @param {Event} event 
   */
  handleDrop(event) {
    this.unfocusDropZone(event)
    if (event.dataTransfer.items) {
      for (const item of event.dataTransfer.items) {
        if (item.kind === 'file') return this.uploadFile(item.getAsFile())
      }
    } else {
      for (const item of event.dataTransfer.files) {
        if (item.kind === 'file') return this.uploadFile(item.getAsFile())
      }
    }
  }

  /** 
   * React to click on drop area
   */
  handleDropClick() {
    this.#showPasteArea = this.#showPasteArea || this.isTypeAccepted('image')
  }

  /**
   * compress File and emit upload event
   * @param {File} file 
   */
  async uploadFile(file) {
    this.#value = ''
    this.#error = ''
    // check type if defined
    if (!this.isTypeAccepted(file.type)) {
      this.#error = 'FileUpload.unsupported-file-type'
      this.updateGui()
    } else {
      // do we have to compress the image?
      const compressImg = (file.type === 'image/jpeg' || file.type === 'image/png') && file.size > this.#maxSize * 1024 * 1024
      // compress png and jpg to max 1MB
      if (compressImg) {
        const options = {
          maxSizeMB: this.#maxSize,
          maxWidthOrHeight: this.#maxDim,
          useWebWorker: false
        }
        try {
          await loadScript('../assets/libs/browser-image-compression.js')
          // eslint-disable-next-line no-undef
          this.#value = await imageCompression(file, options)
        } catch {
          this.#error = 'FileUpload.unsupported-file-type'
        }
        // dont compress other allowed types
      } else {
        if (file.size > this.#maxSize * 1024 * 1024) {
          this.#error = 'FileUpload.max-file-size'
          this.#errorParams = { size: this.#maxSize }
          this.updateGui()
        } else {
          this.#value = file
        }
      }
      // show preview for images
      if (this.isTypeAccepted('image')) {
        const reader = new FileReader()
        this.#showPreview = !!this.#value
        reader.readAsDataURL(this.#value)
        reader.onload = e => {
          this.#previewImg.src = e.target.result
          this.updateGui()
          emit(this, 'input', file)
        }
      }
      else {
        this.updateGui()
        if (!this.#error) emit(this, 'input', file)
      }
    }
  }

  /**
   * Delete selcted image
   */
  deleteImage(e) {
    prevent(e)
    this.#showPreview = false
    this.#previewImg.src = ''
    this.updateGui()
    this.#value = ''
    emit(this, 'input')
  }

  /**
   * display error (public exposed method)
   * @param {Error} e 
   */
  showError(e) {
    this.#error.value = `${e.message}`
    this.#errorParams.value = { 'cause': e.cause }
    this.updateGui()
  }

  /**
   * Get selected file
   * @returns {String}
   */
  get value() {
    return this.#value
  }

  /**
   * Set new value
   * @param {String} new value
   */
  set value(newValue) {
    if (this.isTypeAccepted('image')) {
      this.#showPreview = !!newValue
      this.#previewImg.src = newValue ?? ''
      this.#value = newValue
      this.updateGui()
    }
    this.#value = newValue
  }

  /**
   * Get url encoded image
   */
  get image() {
    return this.#showPreview ? this.#previewImg.src : ''
  }
}


customElements.define('input-file', InputFile)
