import { sitemap } from '../sitemap.js'
import './index-menu.js'
import { HTMLMintElement } from './abstract-components.js'
import { buildHref } from '../modules/utils.js'

/**
 * Component that displays the index menu and uses the information from the sitmap
 */
class MainMenu extends HTMLMintElement {
  connectedCallback() {
    if (this.disconnected) return
    const title = sitemap.title
    const subtitle = sitemap.text
    const menu = Array()
    for (const topic of sitemap.topics) {
      if (topic.id?.startsWith('mint') && topic.icon) {
        const entry = {
          title: topic.title,
          text: topic.text,
          href: buildHref(topic.id),
          icon: `../assets/icons/${topic.icon}`
        }
        menu.push(entry)
      }
    }
    // update view
    const indexMenu = document.createElement('index-menu')
    indexMenu.setAttribute('data-type', 'huge')
    indexMenu.setAttribute('data-translate', true)
    indexMenu.menu = menu
    this.append(indexMenu)
    document.getElementById('page-title').setAttribute('data-title', title)
    document.getElementById('page-title').setAttribute('data-subtitle', subtitle)
  }
}

customElements.define('main-menu', MainMenu)