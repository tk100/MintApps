import { HTMLMintElement } from './abstract-components.js'
import { translateChildren } from '../modules/i18n.js'

/**
 * List of small labels used to explain details in figures
 * @property {String} data-labels - definition of labels, for example '1: Physics.tube, 2: Physics.hot-cathode, 3: Physics.grid, 4: Physics.collector'; each entry starts with a number followed by ':', the second part will be translated using i18n
 * @property {Boolean} data-separate - position canvas text under the graph (takes extra space)
 * @property {Boolean} data-row - layout labels in a row to save space
 */
class CanvasCaption extends HTMLMintElement {
  static observedAttributes = ['data-labels', 'data-separate', 'data-row']

  connectedCallback() {
    if (this.disconnected) return
    if (this.dataset.labes) this.renderLabels()
  }

  attributeChangedCallback() {
    this.innerHTML = ''
    this.renderLabels()
    translateChildren(this)
  }

  /**
   * Render labels according to parameter data-labels
   */
  renderLabels() {
    this.dataset.labels.split(',').forEach(entry => {
      const [key, text] = entry.split(':')
      const div = document.createElement('div')
      const s1 = document.createElement('span')
      s1.innerText = `${key.trim()}`
      div.append(s1)
      text.split('+').forEach(key => {
        const s2 = document.createElement('span')
        s2.setAttribute('data-i18n-md-inline', key.trim() )
        div.append(s2)
      })
      if (this.hasAttribute('data-row')) div.style.display = 'inline-block'
      this.append(div)
    })
    this.classList.add('canvas-caption')
    if (this.dataset.separate === undefined) this.style.position = 'absolute'
  }
}

customElements.define('canvas-caption', CanvasCaption)