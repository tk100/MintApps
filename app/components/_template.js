/**
 * ComponentTemplate
 * @property {String} 'data-xxx' - description of attribut 'data-xxx'
 */
class ComponentTemplate extends HTMLElement {
  /**
   * Array all attributes for which the component needs change notifications
   */
  static observedAttributes = ['data-name']

  constructor() {
    // Always call super first in constructor
    super()
  }

  /**
   * Called each time the component is added to the document,
   * implement custom setup here.
   */
  connectedCallback() {
  }

  /**
   * Called each time the component is removed from the document.
   */
  disconnectedCallback() {
  }

  /**
   * Called when attributes are changed, added, removed, or replaced
   * @param {*} attr 
   * @param {*} oldValue 
   * @param {*} newValue 
   */
  // eslint-disable-next-line no-unused-vars
  attributeChangedCallback(attr, oldValue, newValue) {
    console.log(`Attribute ${attr} has changed.`);
  }

}

// Register element
customElements.define('component-template', ComponentTemplate)