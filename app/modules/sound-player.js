/**
 * Simple sound player that can play sound effects stored in the /src/assets/sound directory.
 * @author Thomas Kippenberg
 * @module sound-player
 */
import index from '../assets/sounds/index.js'

const audios = [] // array to buffer fetched audios
const ctx = new AudioContext() // audio context

/**
 * Play specified sound (completely)
 * @param {string} id - id of sound effect, see /app/assets/sounds/index.js
 */
export async function play (id) {
  if (!index.items[id]) {
    console.error(`Unknown sound with id ${id}`)
    return
  }
  // get mp3 file if not already loaded
  if (!audios[id]) {
    const data = await fetch(`../assets/sounds/${index.items[id].file}`)
    const arrayBuffer = await data.arrayBuffer()
    audios[id] = await ctx.decodeAudioData(arrayBuffer)
  }
  // play
  const playSound = ctx.createBufferSource()
  playSound.buffer = audios[id]
  playSound.connect(ctx.destination)
  playSound.start(ctx.currentTime)
}
