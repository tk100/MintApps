/**
 * Methods to convert two-dimensional arrays to CSV format and back
 * @author Thomas Kippenberg
 * @module csv
*/

export default {
  /**
   * Convert array into csv format
   * @param {string[][]} data - twodimensional data array
   * @returns {string} string in csv format
   */
  data2csv: function (data) {
    let text = ''
    for (const line of data) {
      for (let i = 0; i < line.length; i++) {
        if (typeof line[i] === 'string') {
          // string contains comma? then quote with " and escape " if present
          if (line[i].indexOf(',') > 0) {
            line[i] = '"' + line[i].replaceAll('"', '""') + '"'
          }
        }
        text += line[i]
        if (i < line.length - 1) text += ','
      }
      text += '\n'
    }
    return text
  },

  /**
   * Convert csv format into array
   * @param {string} csv -string in csv format
   * @returns {string[][]} twodimensional data array
   */
  csv2data: function (csv) {
    const data = []
    const lines = csv.split('\n')
    for (let line of lines) {
      line = line.replaceAll('""', '¶') // save double quotes for later restore
      if (line.length > 0) {
        const tmp = []
        let pos = 0
        while (pos < line.length) {
          if (line[pos] === '"') {
            let next = line.indexOf('"', pos + 1)
            if (next < 0) next = line.length
            tmp.push(line.substring(pos + 1, next).replaceAll('¶', '"'))
            pos = next + 2
          } else {
            let next = line.indexOf(',', pos + 1)
            if (next < 0) next = line.length
            tmp.push(line.substring(pos, next))
            pos = next + 1
          }
        }
        data.push(tmp)
      }
    }
    return data
  }
}
