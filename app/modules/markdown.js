/**
 * Module for converting texts in Markdown syntax into HTML. 
 * @author Thomas Kippenberg
 * @module markdown
 * @see module:asciimathml
 */

import { renderMath } from './asciimathml.js'
import { buildHref } from './utils.js'
import { useDecimalSeparatorComma } from './i18n.js'
import domPurify from '../assets/libs/purify.es.min.js'
const domPurifyOptions = { ADD_ATTR: ['target'] }

/**
 * Convert markdown string to html using inline mode
 * @param {string} text - markdown text
 * @param {Boolean} purify - purify HTML to protect against XSS attacks
 * @returns HTMl text
 */
export function renderInline(text, purify = true) {
  if (!text) return ''
  // backwards compability to markdown-it in version 1.0
  text = text.replaceAll("{'@'}", '@').replaceAll("{'|'}", '|')
  // escape ambiguous markers
  text = text.replaceAll('**', '‖')
  let html = ''
  let char, char2
  for (let pos = 0; pos < text.length; pos++) {
    char = text.charAt(pos)
    char2 = pos + 1 < text.length ? char + text.charAt(pos + 1) : ''
    if (char2 === '``') { // inline math
      const pos2 = findMatchingEnd(text, pos, '``')
      let math = text.substring(pos + 2, pos2)
      if (useDecimalSeparatorComma()) math = math.replaceAll('...', '…').replaceAll('.', ',')
      html += renderMath(math, false)
      pos = pos2 + 1
    } else if (char === '`') { // code
      const pos2 = findMatchingEnd(text, pos, '`')
      const part = text.substring(pos + 1, pos2)
      html += `<code>${part}</code>`
      pos = pos2
    } else if (char === '\\') { // escaped characters
      html += text.charAt(pos + 1)
      pos++
    } else if (char === '[') { // hyperlinks
      // get label
      const pos2 = findMatchingEnd(text, pos, '[', ']')
      const label = renderInline(text.substring(pos + 1, pos2))
      // get url
      if (text.charAt(pos2 + 1) === '(') {
        const pos3 = findMatchingEnd(text, pos2 + 1, '(', ')')
        let url = text.substring(pos2 + 2, pos3)
        if (url.startsWith('./')) url = url.substring(2)
        if (url.match(/^mint-[a-z-]+$/)) html += `<a href="${buildHref(url)}">${label}</a>`
        else html += `<a href="${url}" data-target="external" rel="noreferrer" target="_blank">${label}</a>`
        pos = pos3
      }
    } else if (char2 === '![') { // images
      // get label
      pos++
      const pos2 = findMatchingEnd(text, pos, '[', ']')
      const label = text.substring(pos + 1, pos2) // no markdown support for alt text
      // get url
      if (text.charAt(pos2 + 1) === '(') {
        const pos3 = findMatchingEnd(text, pos2 + 1, '(', ')')
        let url = text.substring(pos2 + 2, pos3)
        if (url.startsWith('http')) {
          try {
            console.log(url)
            const u = new URL(url)
            html += `<img src="${u.toString()}" alt="${label}">`
          } catch (e) {
            console.log(e)
            html += 'invalid url'
          }
        }
        else html += `<img src="../assets/icons/${url}" alt="${label}">`
        pos = pos3
      }
    } else if (char === '^') { // sup
      const pos2 = findMatchingEnd(text, pos, '^')
      const part = renderInline(text.substring(pos + 1, pos2))
      html += `<sup>${part}</sup>`
      pos = pos2
    } else if (char2 === '~~') { // strike through
      const pos2 = findMatchingEnd(text, pos, '~~')
      const part = renderInline(text.substring(pos + 2, pos2))
      html += `<s>${part}</s>`
      pos = pos2 + 1
    } else if (char === '~') { // sub
      const pos2 = findMatchingEnd(text, pos, '~')
      const part = renderInline(text.substring(pos + 1, pos2))
      html += `<sub>${part}</sub>`
      pos = pos2
    } else if (char === '‖') { // bold
      const pos2 = findMatchingEnd(text, pos, '‖')
      const part = renderInline(text.substring(pos + 1, pos2))
      html += `<b>${part}</b>`
      pos = pos2
    } else if (char === '*') { // emphasize
      const pos2 = findMatchingEnd(text, pos, '*')
      const part = renderInline(text.substring(pos + 1, pos2))
      html += `<em>${part}</em>`
      pos = pos2
    } else if (char === '<') { // escape
      html += '&lt;'
    } else if (char === '>') { // escape
      html += '&gt;'
    } else if (char === '&') { // escape
      html += '&amp;'
    }
    else html += char
  }
  return purify ? domPurify.sanitize(html, domPurifyOptions) : html
}

/**
 * Convert markdown string to html using block mode;
 * result is wrapped in paragraph(s);
 * line breaks are converted to br-tags
 * @param {string} text - markdown text
 * @param {Boolean} purify - purify HTML to protect against XSS attacks
 * @returns HTMl text
 */
export function renderBlock(text, purify = true) {
  if (!text) return ''
  const lines = text.split('\n')
  let html = ''

  let blockquote = false
  let inTable = false // in table
  let inUlist = false // in unordered list
  let inOlist = false // in ordered list
  let inMath = false  // in math block
  let inCode = false // in code block
  let amBlock = '' // ascii math block
  let codeBlock = '' // code block

  lines.forEach((line, i) => {
    line = line.trim()
    const nextLine = i + 1 < lines.length ? lines[i + 1].trim() : ''
    // headings
    if (line.startsWith('###')) {
      html += `<h3>${renderInline(line.substring(3).trim(), false)}</h3>`
      return
    }
    if (line.startsWith('##')) {
      html += `<h2>${renderInline(line.substring(2).trim(), false)}</h2>`
      return
    }
    if (line.startsWith('#')) {
      html += `<h1>${renderInline(line.substring(1).trim(), false)}</h1>`
      return
    }
    // blockquotes
    if (line.startsWith('>')) {
      if (!blockquote) {
        blockquote = true
        html += '<blockquote>'
      }
      html += `${renderInline(line.substring(1), false)}`
      return
    } else if (blockquote) {
      blockquote = false
      html += '</blockquote>'
    }
    // unordered lists
    if (line.startsWith('- ') && !inTable) {
      if (!inUlist) {
        inUlist = true
        html += '<ul>'
      }
      html += `<li>${renderInline(line.substring(1), false)}</li>`
      return
    } else if (inUlist && !nextLine.match(/^[0-9]+\./) && !nextLine.startsWith('- ')) {
      inUlist = false
      html += '</ul>'
    }
    // ordered lists
    if (line.match(/^[0-9]+\./) && !inTable) {
      if (!inOlist) {
        inOlist = true
        html += '<ol>'
      }
      html += `<li>${renderInline(line.substring(line.indexOf('.') + 1), false)}</li>`
      return
    } else if (inOlist && !nextLine.match(/^[0-9]+\./) && !nextLine.startsWith('- ')) {
      inOlist = false
      html += '</ol>'
    }
    // math blocks
    if (line.startsWith('```am')) {
      inMath = true
      amBlock = ''
      return
    } else if (line.startsWith('```') && inMath) {
      inMath = false
      html += `<div class="text-center">${renderMath(amBlock, true)}</div>`
      return
    } else if (inMath) {
      amBlock += line + '\n'
      return
    }
    // code blocks
    if (line.startsWith('~~~') && !inCode) {
      inCode = true
      codeBlock = ''
      return
    } else if (line.startsWith('~~~') && inCode) {
      inCode = false
      html += `<pre>${codeBlock}</pre>`
      return
    } else if (inCode) {
      codeBlock += line + '\n'
      console.log(codeBlock)
      return
    }
    // horizontal rule
    if (line === '---') {
      html += '<hr>'
      return
    }
    // tables
    if (nextLine.match(/---[ ]*|[ ]*---/) && nextLine !== '---') {
      inTable = true
      html += '<table><thead><tr>'
      line.split('|').forEach(th => {
        html += `<th>${renderInline(th.trim(), false)}</th>`
      })
      html += '</tr></thead>'
      return
    } else if (line.match(/---[ ]*|[ ]*---/)) {
      html += '<tbody>'
      return
    } else if (inTable && line.indexOf('|') > 0) {
      html += '<tr>'
      line.split('|').forEach(td => {
        html += `<td>${renderInline(td.trim(), false)}</td>`
      })
      html += '</tr>'
      return
    } else if (inTable) {
      html += '</tbody></table>'
      inTable = false
    }
    if (line) html += `<p>${renderInline(line, false)}</p>`
  })
  if (inTable) html += '</table>'
  if (inUlist) html += '</ul>'
  return purify ? domPurify.sanitize(html, domPurifyOptions) : html
}

/**
 * Search for the specified end token within the text
 * @param {String} text - complete line of text
 * @param {Number} pos - start position for search
 * @param {String} start - start token, for example (
 * @param {String} [end=start] - closing token, for example )
 * @returns {Number} position
 */
function findMatchingEnd(text, pos, start, end = start) {
  pos++
  let level = 1
  const startLength = start.length
  const endLength = end.length
  while (pos < text.length) {
    if (text.substring(pos, pos + endLength) === end) {
      // closing element found
      if (--level === 0) return pos
      pos++
    } else if (text.substring(pos, pos + startLength) === start) {
      // increase nesting level
      level++
      pos += startLength
    } else {
      pos++
    }
  }
  // not found, return end of line
  return pos
}