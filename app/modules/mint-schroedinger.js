/**
 * Module that contains the potential function for the app mint-schroedinger.
 * Energy-unit: eV, space-unit: m
 * @author Thomas Kippenberg
 * @module mint-schroedinger
 */

const Q_E = 1.602177e-19 // elementary charge
const M_E = 9.109384e-31 // electron mass
const M_ALPHA = 6.644657e-27 // mass alpha particle
const EPS_0 = 8.854188e-12 // electric field constant
const h = 6.62607e-34 // planck constant

// Energy-unit: eV
// Space-unit: m

/**
 * finitely high, rectangular potential well
 */
const finiteSquarePotential = {
  id: 'finite-square-potential',
  m: M_E,
  E0: 0,
  E1: 11,
  stepE: 0.001,
  x0: -10,
  x1: 10,
  a: 10,
  V0: 10,
  radial: false,
  V: function (x) {
    return x > -0.5 * finiteSquarePotential.a &&
      x < 0.5 * finiteSquarePotential.a
      ? 0
      : finiteSquarePotential.V0
  },
  params: [
    { id: 'V0', math: 'V_0', min: 0.1, max: 10, value: 10, unit: 'eV' },
    { id: 'a', math: 'a', min: 0.1, max: 20, value: 10, unit: 'Å' }
  ]
}

/**
 * infinitely high, rectangular potential well
 */
const infiniteSquarePotential = {
  id: 'infinite-square-potential',
  m: M_E,
  E0: 0,
  E1: 11,
  stepE: 0.001,
  x0: -10,
  x1: 10,
  a: 10,
  V0: 1E6,
  radial: false,
  V: function (x) {
    return x > -0.5 * infiniteSquarePotential.a &&
      x < 0.5 * infiniteSquarePotential.a
      ? 0
      : infiniteSquarePotential.V0
  },
  params: [
    { id: 'a', math: 'a', min: 0.1, max: 20, value: 10, unit: 'Å' }
  ],
  psi: function (x, E) {
    const k = (Math.sqrt(E * 8 * M_E) * Math.PI) / h
    const yRight = Math.sin(1e-10 * k * infiniteSquarePotential.a)
    if (x < -0.5 * infiniteSquarePotential.a) return 0
    if (x <= 0.5 * infiniteSquarePotential.a) { return Math.sin(1e-10 * k * (x + 0.5 * infiniteSquarePotential.a)) }
    if (Math.abs(yRight) < 1e-2) return 0
    return NaN
  }
}

/**
 * V-shaped potential well
 */
const vShapedPotential = {
  id: 'v-shaped-potential',
  m: M_E,
  E0: 0,
  E1: 10,
  stepE: 0.001,
  x0: -5,
  x1: 5,
  a: 10,
  radial: false,
  V: function (x) {
    return (Math.abs(x) * vShapedPotential.a) / 5
  },
  params: [
    { id: 'a', math: 'a', min: 10, max: 20, value: 10, unit: 'eVÅ⁻¹' }
  ]
}

/**
 * Double, finite height, rectangular potential well
 */
const doubleSquarePotential = {
  id: 'double-square-potential',
  m: M_E,
  E0: 0,
  E1: 10,
  x0: -5,
  x1: 5,
  radial: false,
  V: function (x) {
    return (x > -4 && x < -1) || (x > 1 && x < 4) ? 0 : 10
  }
}

/**
 * Harmonic / parabolic potential
 */
const harmonicPotential = {
  id: 'harmonic-potential',
  m: M_E,
  E0: 0,
  E1: 10,
  stepE: 0.01,
  x0: -5,
  x1: 5,
  radial: false,
  V: function (x) {
    return x ** 2 * harmonicPotential.a
  },
  a: 0.5,
  params: [
    { id: 'a', math: 'a', min: 0.4, max: 2, value: 0.5, step: 0.1, unit: 'evÅ⁻²' }
  ]
}

/**
 * finitely high potential well with sloping bottom
 */
const inclinedPotential = {
  id: 'inclined-potential',
  m: M_E,
  E0: 0,
  E1: 20,
  stepE: 0.001,
  x0: -8,
  x1: 8,
  a: 12,
  h: 10,
  V0: 20,
  radial: false,
  V: function (x) {
    return x > -0.5 * inclinedPotential.a && x < 0.5 * inclinedPotential.a
      ? (inclinedPotential.h * (x + 0.5 * inclinedPotential.a)) /
          inclinedPotential.a
      : inclinedPotential.V0
  },
  params: [
    { id: 'V0', math: 'V_0', min: 0.1, max: 20, value: 20, unit: 'eV' },
    { id: 'a', math: 'a', min: 0.1, max: 15, value: 10, unit: 'Å' },
    { id: 'h', math: 'h', min: 0, max: 10, value: 1, unit: 'eV' }
  ]
}

/**
 * Potential of the hydrogen atom for the angular momentum quantum number l=0
 */
const hydrogenPotential = {
  id: 'hydrogen-potential',
  m: M_E,
  E0: -15,
  E1: 1,
  stepE: 0.002,
  x0: 0,
  x1: 15,
  radial: true,
  params: [
    { id: 'x1', math: 'r_"max"', min: 10, max: 50, value: 15, step: 5, unit: 'Å', slider: true }
  ],
  V: function (x) {
    const V0 = -Q_E / (4 * Math.PI * EPS_0 * Math.abs(x * 1e-10))
    return V0
  }
}

/**
 * Potential of an atomic nucleus to represent the tunneling effect of alpha particles
 */
const nuclearPotential = {
  id: 'nuclear-potential',
  m: M_ALPHA,
  E0: -10e6,
  x0: 0e-5,
  E1: +15e6,
  stepE: 1e3,
  x1: 15e-5,
  radial: true,
  V: function (x) {
    const V0 = -9e6
    const V1 = (60 * Q_E) / (4 * Math.PI * 8.85e-12 * Math.abs(x * 1e-10))
    return x < 6e-5 ? V0 : V1
  }
}

const potentialList = [
  finiteSquarePotential,
  infiniteSquarePotential,
  inclinedPotential,
  doubleSquarePotential,
  vShapedPotential,
  harmonicPotential,
  hydrogenPotential,
  nuclearPotential
]

export { potentialList }
