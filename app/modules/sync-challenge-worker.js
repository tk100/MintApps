/**
 * solve cryptographic challenge provided by the server
 * @see module:sync.js for more details
 * don't use without proper validation of the challenge parameter
 * @module sync-challenge-worker
 * @param {string} data.challenge - complete cryptographic challenge.
 * example: '4:Sj5YxqY4Y2:1723483235231', 
 * first part = number of requested zeros, second part = salt, third pard = timestamp
 */
self.onmessage = async ({ data: { challenge } }) => {
  const strength = Number(challenge.substring(0, challenge.indexOf(':')))
  const zeros = '0'.repeat(strength)
  let result
  let counter = 0
  const maxCounter = Math.pow(16, strength + 1)
  const encoder = new TextEncoder()
  while (!result) {
    const tmp = challenge + ':' + counter
    const data = encoder.encode(tmp)
    const hashBuffer = await crypto.subtle.digest('SHA-256', data)
    const hashArray = Array.from(new Uint8Array(hashBuffer))
    const hashLength = hashArray.length
    const hashHexEnd = // Works up to 6 trailing zeros - which is rather a HUGE value, recommended are 3 to 4 trailing zeros
      hashArray[hashLength - 3].toString(16).padStart(2, '0') +
      hashArray[hashLength - 2].toString(16).padStart(2, '0') +
      hashArray[hashLength - 1].toString(16).padStart(2, '0')
    if (hashHexEnd.endsWith(zeros)) result = tmp // challenge solved :-)
    if (counter % 10000 === 0) { self.postMessage({ progress: counter / maxCounter }) }
    if (counter > maxCounter) result = tmp // return false challenge, server will reject it, and client will display corresponding message
    counter++
  }
  self.postMessage({ solution: result })
}
