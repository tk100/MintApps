/**
 * Module containing calculations for the app mint-vidana
 * @author Thomas Kippenberg
 * @module mint-vidana
 */

/**
 * Calculate distance of points a and b
 * @param {object} a - coordinates of point a {x, y}
 * @param {object} b  - coordinates of point b {x, y}
 * @returns {number} distance
 */
export function distancePoints(a, b) {
  const d = Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
  return d
}

/**
 * Calculate data from measured points
 * @param {object} params - parameter object
 * @param {object} params.axis1 - direction of axis 1 {x, y}
 * @param {object} params.axis2 - direction of axis 2 {x, y}
 * @param {object} params.origin - origin of coordinate system {x, y}
 * @param {number} params.time0 - time origin
 * @param {number} params.frameRate - frames per second
 * @param {number} params.scale - length of scale in m
 * @param {object} params.scale1 - start of scale object
 * @param {object} params.scale2 - end of scale object
 * @param {number[]} params.frames - timestamps of frames in ms
 * @param {object[]} params.points - points identified by user
 * @returns {object[]} array containing results as objects { t, x, y, vx, vy, ax, ay }
 */
export function dataFromPoints(params) {
  const origin = params.origin
  const a = params.axis1
  let b
  if (params.axis2) {
    b = params.axis2 // two-dimensional coord system
  } else {
    b = { x: -(a.y - origin.y) + origin.x, y: +(a.x - origin.x) + origin.y } // one-dimensional coord system, choose b perpendicular to a
  }
  const points = params.points
  const data = []
  // normalize axis length
  const la = Math.sqrt((a.x - origin.x) ** 2 + (a.y - origin.y) ** 2)
  const ax = (a.x - origin.x) / la
  const ay = (a.y - origin.y) / la
  const lb = Math.sqrt((b.x - origin.x) ** 2 + (b.y - origin.y) ** 2)
  const bx = (b.x - origin.x) / lb
  const by = (b.y - origin.y) / lb

  // order by time
  points.sort((a, b) => a.t - b.t)

  // calc final coordinates, using interpolated and equidistant time values
  const iTime0 = params.frames.indexOf(params.time0) / params.frameRate
  for (const point of points) {
    const iTime = params.frames.indexOf(point.t) / params.frameRate
    const t = iTime - iTime0
    // translate origin
    const px = point.x - origin.x
    const py = point.y - origin.y
    // intersection point with a axis
    let x = -(bx * py - by * px) / (ax * by - ay * bx)
    let y = +(ax * py - ay * px) / (ax * by - ay * bx)
    // scale
    const scale = params.scale / distancePoints(params.scale1, params.scale2)
    x *= scale
    y *= scale
    data.push({ t, x, y, vx: NaN, vy: NaN })
  }
  // derivate
  derivate(data, 'x', 't', 'vx')
  derivate(data, 'y', 't', 'vy')
  derivate(data, 'vx', 't', 'ax')
  derivate(data, 'vy', 't', 'ay')
  return data
}

/**
 * numerical derivation
 * @param {object[]} data - array of objects { t, x, y, vx, vy, ax, ay }
 * @param {string} y - id used as y-value
 * @param {string} x - id used as x-value
 * @param {string} dst - id used for the result
 * @returns {object[]} modified data object { t, x, y, vx, vy, ax, ay }
 */
export function derivate(data, y, x, dest) {
  const numData = data.length
  if (numData === 0) return
  for (let i = 0; i < numData; i++) {
    if (
      i === 0 ||
      i === numData - 1 ||
      isNaN(data[i][x]) ||
      isNaN(data[i - 1][x]) ||
      isNaN(data[i + 1][x]) ||
      isNaN(data[i][y]) ||
      isNaN(data[i - 1][y]) ||
      isNaN(data[i + 1][y])
    ) {
      data[i][dest] = NaN
    } else {
      data[i][dest] =
        (0.5 * (data[i][y] - data[i - 1][y])) /
        (data[i][x] - data[i - 1][x]) +
        (0.5 * (data[i + 1][y] - data[i][y])) / (data[i + 1][x] - data[i][x])
    }
  }
}

