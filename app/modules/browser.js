/**
 * This file contains functions to query browser-specific details
 * @author Thomas Kippenberg
 * @module browser
*/

/**
 * Determine the generic browser name by evaluating the user agent string 
 * @returns {string} Abbreviation of the browser name (opera | chrome | msie | firefox | safari)
 */
export function getBrowserName () {
  const userAgentString = navigator.userAgent
  if (userAgentString.indexOf('OP') > -1) return 'opera'
  else if (userAgentString.indexOf('Chrome') > -1) return 'chrome'
  else if (userAgentString.indexOf('MSIE') > -1) return 'msie'
  else if (userAgentString.indexOf('Firefox') > -1) return 'firefox'
  else if (userAgentString.indexOf('Safari') > -1) return 'safari'
  else return ''
}
