/**
 * Different states for the physical simulations
 * @author Thomas Kippenberg
 * @module states
 */

/**
 * Initial state of simulation
 */
const sInit = 0
/**
 * Simulation is running
 */
const sRunning = 1
/**
 * Simulation is paused
 */
const sPaused = 2
/**
 * The simulated process is completed
 */
const sDone = 3
/**
 * Exactly one time step is simulated
 */
const sStep = 4

export { sInit, sRunning, sPaused, sDone, sStep }
