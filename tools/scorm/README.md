# Create download packages

This directory contains scripts to create the SCORM packages, which can be integrated into learning management systems (LMS) such as Moodle.

## Steps

Follow the instructions in [INSTALL.md](https://codeberg.org/MintApps/client/src/branch/main/INSTALL.md) up to and including the "Download" section. Then run the following commands:

~~~
npm install
npm run scorm
~~~

## Notes

- Build process may take a long time (up to an hour).
- To speed up the process, only missing or outdated packages are generated.
- You will find the generated packages in `app/assets/download`.
- Packages automatically detect the users preferred language and theme (light / dark)
- Only the default Page Tab is visible, usually this is the tab "Simulation"
- The generated SCORM packages are very simple. Functions such as progress, repetition etc. are not supported. 
- Additionally: if you run into issues about a missing weblate directory, modify build.js (l. 16) accordingly
