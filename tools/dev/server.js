import express from 'express'

const app = express()
app.use(express.static('app'))
app.listen(8080, () => {
  console.log('Dev server on port 8080')
})
