# To use MintApps with local docker

Dockerisation of MintApps client, weblate and data repositories

- Usage: `docker-compose up -d`
- Go to browser and enter: `http://localhost/`
